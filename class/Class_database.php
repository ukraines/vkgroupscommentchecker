<?php
	function _p ($str) {
		return preg_replace('/ {2,}/',' ',$str);
	}

	class database {
		// перед использованием нужно создать базу с именем, а таблица будет создана автоматически.
		
		public 	$db_table;
		private $db_dbname = 'demo_demo';
		private $db_host = 'localhost';
		private $db_login = 'demo_demo';
		private $db_password = 'VVMfd5A5U6';

		public function db_connect () {
			try {
				$connect = new PDO("mysql:host=" . $this->db_host . ";dbname=" . $this->db_dbname, $this->db_login, $this->db_password);
			} catch (PDOException $err) {
				return false;
			}
			return $connect;
		}

		public function __construct ($tableName = '') {
			if(!$this->db_connect()) 
				die('Error connect to DB.');

			if ($tableName != '') {
				$this->db_table = $tableName;
				
				$table = $this->tableCheck();

				if (!$table)
					$this->tableCreate($tableName);
			}
		}

		protected function tableCheck () {

			$tableName = $this->db_table;
			$dbName = $this->db_dbname;

			$query = $this->sql_query("SHOW TABLES FROM `" . $dbName . "`");
			$list_table = $query;
			
			foreach ($list_table as $key => $value) {
				if ($value == $tableName)
					return true; // таблица присутствует
			}

			return false; // таблицы нет, нужно создать

		}

		public function getData () {
			
			$tableName = $this->db_table;

			$query = $this->sql_query('SELECT * FROM `'.$tableName.'`');

			return $query;
		}

		public function addData ($data_array, $dupl_check = true) { // фк-ция добавления данных в бд
			
			$tableName = $this->db_table;
			$columns = $this->getColumns(); // получаем все колонки котоыре имеются в бд
			$values = '';
			$names = '';

			foreach ($data_array as $key => $value) { // проверяем, если не имеется в бд колонки, которая есть в массиве, то добавляем её в бд
				if (!in_array($key, $columns))
					$this->alterColumn($key);
				$names .= '`' . $key . '`,' ; // формируем колонки для вставки в бд
				$values .= "'" . mysql_escape_string(trim(_p(preg_replace("/(.*?)<br>$/is", "$1", $value)))) . "'," ; // формируем данные для вставки в бд	
			}
			
			$names = substr($names, 0, strlen($names)-1); // обрезаем в конце запятую лишнюю
			$values = substr($values, 0, strlen($values)-1); // обрезаем в конце запятую лишнюю

			$query_str = "INSERT INTO `" . $tableName . "` (" . $names . ") VALUES(" . $values . ")";
			
			if ($dupl_check) {
				$dupl = $this->checkDuplicate($data_array);
				if ($dupl)
					$this->sql_exec($query_str);
				else
					return 0;
			}else{
				$this->sql_exec($query_str);
			}

		}

		public function checkDuplicate ($data_array) {

			$tableName = $this->db_table;
			$query_str = "SELECT ";

			foreach ($data_array as $key => $value) {
				$query_str .= '`'.$key.'`,';
			}

			$query_str = substr($query_str, 0, strlen($query_str)-1);
			$query_str .= " FROM ".$tableName." WHERE ";
			
			foreach ($data_array as $key => $value) {
				$query_str .= "`".$key."`='". mysql_escape_string(trim(_p(preg_replace("/(.*?)<br>$/is", "$1", $value)))) ."' AND ";
			}

			$query_str = substr($query_str, 0, strlen($query_str)-4);

			$query = $this->sql_query($query_str);
			
			if (empty($query))
				return true;
			return false;
		}

		public function alterColumn ($column, $params = array('type'=>'TEXT','text'=>'null')) {

			$tableName = $this->db_table;

			$query = $this->sql_exec("ALTER TABLE `" . $tableName . "` ADD " . $column . " " . $params['type'] . " " . $params['text']);
		}
		
		public function tableList () {

			$query = $this->sql_query("SHOW TABLES");

			return $query;
		}

		public function getColumns ($fullinfo = false) {

			$tableName = $this->db_table;

			$query = $this->sql_query("DESCRIBE `" . $tableName . "`");
			
			$columns = array();

			if ($fullinfo == false) {
				foreach ($query as $key => $value) {
					$columns[] = $value['Field'];
				}

				return $columns;
			}else
				return $query;
			
		}

		protected function tableCreate () {	

			$tableName = $this->db_table;
			$this->sql_query("CREATE TABLE `" . $tableName . "` (id serial) COLLATE=utf8_general_ci ENGINE=InnoDB;");
		
		}

		public function sql_exec ($query, $params = array()) {
			
			$pdo = $this->db_connect();

			$sql = $pdo->prepare($query);
			$sql->execute($params);

		}

		public function sql_query ($query, $params = array(), $type = PDO::FETCH_ASSOC) {
			
			$pdo = $this->db_connect();

			$sql = $pdo->prepare($query);
			$sql->execute($params);

			return $sql->fetchAll($type);
		}
	}
?>