﻿<?php
class group{
	
	private $group_id;
	private $wall_count;
	private $topic_count;	
	private $topic_mas;	
	private	$wall_mas;	

	public function __construct ($id = 0) {		
		$wall_count=0;
		if(!$id) die('error num group');		
		$this->group_id = $id;	
	}

	public function load_info_topic($num_offset){		

			if($num_offset==0)$offset=''; else $offset='&offset='.$num_offset*100;
			$url = 'http://api.vk.com/method/board.getTopics?group_id='.$this->group_id.$offset.'&count=100';				
			$code = json_decode(request_get($url),true);						
			$code=$code['response'];	
			$code=$code['topics'];	
			$i=0;
			foreach ($code as  $key => $value )
			{
				if($num_offset==0 AND $key == 'count') $this->topic_count=$value;								
					
				if(isset($value['tid'])){
	
					$this->topic_mas[$i]['id']=$value['tid'];
					$this->topic_mas[$i]['title']=$value['title'];
					$this->topic_mas[$i]['created']=$value['created'];
					$this->topic_mas[$i]['created_by']=$value['created_by'];
					$this->topic_mas[$i]['updated']=$value['updated'];
					$this->topic_mas[$i]['updated_by']=$value['updated_by'];
					$this->topic_mas[$i]['comments']=$value['comments'];
					$i++;						
				}									
			}	
		return $this->topic_mas;
	}
	
	public function load_info_wall($num_offset){		

			if($num_offset==0)$offset=''; else $offset='&offset='.$num_offset*100;

			$url = 'http://api.vk.com/method/wall.get?owner_id=-'.$this->group_id.$offset.'&count=100';			
			$code = json_decode(request_get($url),true);
			$code=$code['response'];	
			$i=0;
			foreach ($code as $key => $value ){
				if($num_offset==0 AND $key == 'count') $this->wall_count=$value;				
										
					$tmp = $value['comments'];

					if(isset($value['id'])){					

						$this->wall_mas[$i]['id']=$value['id'];
						$this->wall_mas[$i]['count']=$tmp['count'];						
						$this->wall_mas[$i]['from_id']=$value['from_id'];
						$this->wall_mas[$i]['date']=$value['date'];
						$this->wall_mas[$i]['text']=$value['text'];
						$i++;						
				  	}					
								
			}
			
		return $this->wall_mas;
	}

	public function get_comment_wall($post_id,$com_count_db,$upd_data){
		$url = 'http://api.vk.com/method/wall.getComments?owner_id=-'.$this->group_id.'&post_id='.$post_id.'&offset='.$com_count_db;
		$code = json_decode(request_get($url),true);		
		$code=$code['response'];
		$i=0;
		foreach ($code as $coment )		
		{
		  if(isset($coment['uid']) AND $coment['date']>$upd_data) {
			$url = 'http://api.vkontakte.ru/method/getProfiles?uids='.$coment['uid'];
			$usr = json_decode(request_get($url),true);
			$usr = $usr['response'];
			$usr = $usr[0];	
			$mas[$i]['uid']='<a href=http://vk.com/id'.$coment['uid'].'>'.$usr['first_name'].' '.$usr['last_name'].'</a>';
			$mas[$i]['date']=$coment['date'];
			$mas[$i]['text']=$coment['text'];
			$mas[$i]['link']='<a href=http://vk.com/wall-'.$this->group_id.'_'.$post_id.'?reply='.$coment['cid'].' >comment_w</a>';
			$i++;
			}
		}
	return $mas;
	}


	public function get_comment_top($topic_id,$com_count_db,$upd_data){
		$url = 'http://api.vk.com/method/board.getComments?group_id='.$this->group_id.'&topic_id='.$topic_id.'&offset='.$com_count_db;
		$code = json_decode(request_get($url),true);		
		$code=$code['response'];
		$code=$code['comments'];
		$i=0;		
		foreach ($code as $key => $coment )		
		{
		  if(isset($coment['id']) AND $coment['date']>$upd_data) {
			$url = 'http://api.vkontakte.ru/method/getProfiles?uids='.$coment['id'];
			$usr = json_decode(request_get($url),true);
			$usr = $usr['response'];
			$usr = $usr[0];	
			$mas[$i]['uid']='<a href=http://vk.com/id'.$coment['from_id'].'>'.$usr['first_name'].' '.$usr['last_name'].'</a>';
			$mas[$i]['date']=$coment['date'];
			$mas[$i]['text']=$coment['text'];
			$mas[$i]['link']='<a href=http://vk.com/topic-'.$this->group_id.'_'.$topic_id.'?post='.$coment['id'].' >comment_t</a>';
			$i++;
			}
		}
	return $mas;
	}



	public function get_post_w($id){
	
		foreach ($this->wall_mas as $m )
		{		
	  		if($m['id']==$id){

				$idusr=explode("-",$m['from_id']);
				if($idusr[0]!=''){

				$url = 'http://api.vkontakte.ru/method/getProfiles?uids='.$idusr[0];
				$usr = json_decode(request_get($url),true);
				$usr = $usr['response'];
				$usr = $usr[0];
				$userp ='<a href=http://vk.com/id'.$idusr[0].'>'.$usr['first_name'].' '.$usr['last_name'].'</a>';
				}else{
					$userp = '<a href=http://vk.com/id101>Администроатор</a>';
				}
	
				$mas['uid']= $userp;
				$mas['date']=$m['date'];
				$mas['text']=$m['text'];
				$mas['link']='<a href=http://vk.com/wall-'.$this->group_id.'_'.$m['id'].' >wall</a>';
			}	
		}
	return $mas;
	}


	public function get_post_t($id){
	
		foreach ($this->topic_mas as $m )
		{		
	  		if($m['id']==$id){
				$idusr=explode("-",$m['created_by']);
				if($idusr[0]!=''){

				$url = 'http://api.vkontakte.ru/method/getProfiles?uids='.$idusr[0];
				$usr = json_decode(request_get($url),true);
				$usr = $usr['response'];
				$usr = $usr[0];
				$userp ='<a href=http://vk.com/id'.$idusr[0].'>'.$usr['first_name'].' '.$usr['last_name'].'</a>';
				}else{
					$userp = '<a href=http://vk.com/id101>Администроатор</a>';
				}
	
				$mas['uid']= $userp;
				$mas['date']=$m['created'];
				$mas['text']=$m['title'];
				$mas['link']='<a href=http://vk.com/topic-'.$this->group_id.'_'.$m['id'].' >topic</a>';
			}	
		}
	return $mas;
	}


	public function get_new_w_in_db($id){
	
		foreach ($this->wall_mas as $m )
		{
	  		if($m['id']==$id){
	
				$mas['groups_id']=$this->group_id;
				$mas['posttop_id']=$m['id'];
				$mas['comment_count']=0;
				$mas['upd']=0;
			}	
		}
	
	return $mas;
	}	


	public function get_new_t_in_db($id){
	
		foreach ($this->topic_mas as $m )
		{
	  		if($m['id']==$id){
	
				$mas['groups_id']=$this->group_id;
				$mas['posttop_id']=$m['id'];
				$mas['comment_count']=0;
				$mas['upd']=0;
			}	
		}
	
	return $mas;
	}	
	public function get_name()
	{
		$url = 'http://api.vk.com/method/groups.getById?group_id='.$this->group_id;
		$code = json_decode(request_get($url),true);						
		$code=$code['response'];	
		return $code[0]['name'];
	}
	public function get_num_wall(){return $this->wall_count;}
	public function get_num_topic(){return $this->topic_count;}
	
}

?>