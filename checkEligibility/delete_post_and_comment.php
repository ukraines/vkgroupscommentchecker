<?php

set_time_limit(1200);

define('DB_PERSISTENCY', true); // Постоянное соединения
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'password');
define('DB_DATABASE', 'vkgroups');
define('DB_CHARSET', 'utf8');
define('PDO_DSN', 'mysql:host=' . DB_SERVER . ';dbname=' . DB_DATABASE . ';charset=' . DB_CHARSET);

define('VK_ACCESS_TOKEN','de7414de1543f89644d2afe8a76746f25722618462527c0aceb4e3711eb312bc02e2c37944b01e4420473');

define('COUNT_VIOLATION', 4); // Максимальное число нарушений в групе

users_vk::init();

$groups = [ 
    '127181074',
];

$users_ban = [ // Ключ - ID групы
    '127181074' => [
        '372572565', // Значения  - массив ID пользователей
        '203369041'
    ], 
];

$messages_template = [

    1 => 'Здравствуйте %first_name%. 
	Вынуждены удалить ваше сообщение из-за несоответствия правилам группы %group%. Публикация допускается только подписчиками. 
	Подсказка: подписываемся на группу, после чего можно опубликовать свое сообщение и/или комментарий.
	Если вас интересует реклама в группе, раскрутка вашей группы, обращайтесь.
	Спасибо за понимание',

    2 => 'Здравствуйте %first_name%. 
	Неприятно, но ваше сообщение было снова удалено из группы %group%. Чтобы Ваше будущее сообщение прошло модерацию необходимо быть подписчиком группы. 
	Если вас интересует реклама в нашей группе или раскрутка вашей, обращайтесь.
	Спасибо за уделенное время',

    3 => 'Здравствуйте %first_name%,
	Мы снова удалили ваше сообщение в %group%. Напоминаем, чтобы ваши будущие публикации проходили модерацию необходимо быть подписчиком группы. 
	Если вас интересует реклама в группе, раскрутка вашей или разработка рекламной кампании, обращайтесь.
	Спасибо за уделенное время',

    4 => 'Желаем вам удачи в бизнесе )', 
];

// Сообщения спама
$message_complaint = 'Пользователь распространяет спам';

$posts = [];

foreach($users_ban as $key => $value){
    if(!in_array($key, $groups)){
        $groups [] = $key;
    }
}

$groups = array_unique($groups);

foreach ($groups as $group) {
    $offset = 0;
    $status = true;
    $last_date = 0;

    while ($offset <= 400 && $status) {
        $status = false;

        $result = json_decode(vkWallGet('-' . $group, $offset));
        
        if ($result && is_object($result) && isset($result->response) && is_object($result->
            response) && isset($result->response->count) && $result->response->count > 0 &&
            isset($result->response->items)) {
            foreach ($result->response->items as $item) {
                $last_date = $item->date;

                if ((time() - $item->date) < 86400) {
                    $item->type = 'wall';

                    $posts[$group][] = $item;
                }

                if (isset($item->comments) && isset($item->comments->count) && $item->comments->
                    count > 0) {
                    $result = json_decode(vkWallGetComments('-' . $group, $item->id));

                    if ($result && is_object($result) && isset($result->response) && is_object($result->
                        response) && isset($result->response->count) && $result->response->count > 0 &&
                        isset($result->response->items)) {
                        foreach ($result->response->items as $item) {
                            if ((time() - $item->date) < 86400) {
                                $item->type = 'comment';

                                $posts[$group][] = $item;
                            }
                        }
                    }
                }
            }
        }

        if (time() - $last_date < 86400) {
            $status = true;

            $offset = $offset + 50;
        }
    }

    $result = json_decode(vkBoardGetTopics($group));

    if ($result && is_object($result) && isset($result->response) && is_object($result->
        response) && isset($result->response->count) && $result->response->count > 0 &&
        isset($result->response->items)) {
        foreach ($result->response->items as $item) {
            if ($item->created_by != 101 && (time() - $item->created) < 86400) {
                $item->type = 'topic';
                $item->from_id = $item->created_by;

                $posts[$group][] = $item;
            }

            $topic_id = $item->id;

            $offset = 0;
            $status = true;
            $last_date = 0;

            while ($offset <= 400 && $status) {
                $status = false;
                $result = json_decode(vkBoardGetComments($group, $topic_id, $offset));

                if ($result && is_object($result) && isset($result->response) && is_object($result->
                    response) && isset($result->response->count) && $result->response->count > 0 &&
                    isset($result->response->items)) {
                    foreach ($result->response->items as $item) {
                        $last_date = $item->date;

                        if ((time() - $item->date) < 86400) {
                            $item->type = 'board';
                            $item->topic_id = $topic_id;
                            $posts[$group][] = $item;
                        }
                    }
                }

                if (time() - $last_date < 86400) {
                    $status = true;
                    $offset = $offset + 50;
                }
            }
        }
    }
}

$stack = array();

foreach ($posts as $owner_id => $messages) {
    foreach ($messages as $message) {
        if ($message->from_id != 101) {
            users_vk::setId($message->from_id);
        }
    }

    users_vk::process($owner_id);

    if (true) {
        foreach ($messages as $message) {
            if (isset($users_ban[$owner_id])) {
                if (in_array($message->from_id, $users_ban[$owner_id])) {
                    if ($message->type == 'wall') {
                        vkWallDelete('-' . $owner_id, $message->id);

                        $stack[] = "Удален пост от " . $message->from_id;
                    } elseif ($message->type == 'board') {
                        vkBoardDeleteComment($owner_id, $message->topic_id, $message->id);

                        $stack[] = "Удален комментарий в обсуждении от " . $message->from_id;
                    } elseif ($message->type == 'topic') {
                        vkBoardDeleteTopic($owner_id, $message->id);

                        $stack[] = "Удалена тема от " . $message->from_id;
                    } elseif ($message->type == 'comment') {
                        vkWallDeleteComment('-' . $owner_id, $message->id);

                        $stack[] = "Удален комментарий к посту пользователя" . $message->from_id;
                    }
                }
            } else {
                if ($message->from_id != 101 && users_vk::isViolator($message->from_id, $owner_id)) {
                    $first_name = "";
                    $last_name = "";

                    $user_inf = users_vk::getName($message->from_id);

                    $count_violation = users_vk::store($message->from_id, $owner_id, time());

                    if ($message->type == 'wall') {
                        vkWallDelete('-' . $owner_id, $message->id);

                        $stack[] = "Удален пост от " . $user_inf['first_name'] . " " . $user_inf['last_name'] .
                            " (" . $message->from_id . ")(" . $owner_id . ")(" . $count_violation . ")";
                    } elseif ($message->type == 'board') {
                        vkBoardDeleteComment($owner_id, $message->topic_id, $message->id);

                        $stack[] = "Удален комментарий в обсуждении от " . $user_inf['first_name'] . " " .
                            $user_inf['last_name'] . " (" . $message->from_id . ")(" . $owner_id . ")(" . $count_violation .
                            ")";
                    } elseif ($message->type == 'topic') {
                        vkBoardDeleteTopic($owner_id, $message->id);

                        $stack[] = "Удалена тема от " . $user_inf['first_name'] . " " . $user_inf['last_name'] .
                            " (" . $message->from_id . ")(" . $owner_id . ")(" . $count_violation . ")";
                    } elseif ($message->type == 'comment') {
                        vkWallDeleteComment('-' . $owner_id, $message->id);

                        $stack[] = "Удален комментарий к посту " . $user_inf['first_name'] . " " . $user_inf['last_name'] .
                            " (" . $message->from_id . ")(" . $owner_id . ")(" . $count_violation . ")";
                    }

                    if ($count_violation == COUNT_VIOLATION) {
                        vkUsersReport($message->from_id, 'spam', $message_complaint);
                    }

                    if (isset($messages_template[$count_violation])) {
                        $text = str_replace(['%first_name%', '%last_name%', '%group%'], [$user_inf['first_name'],
                            $user_inf['last_name'], 'https://vk.com/club' . $owner_id], $messages_template[$count_violation]);

                        vkMessagesSend($message->from_id, $text);
                    }
                }
            }
        }
    }
}

echo implode("<br />", $stack);

// Позволяет пожаловаться на пользователя.
function vkUsersReport($user_id, $type, $comment)
{
    $url = 'https://api.vk.com/method/users.report';
    $params = array(
        'user_id' => $user_id,
        'type' => $type,
        'comment' => $comment,
        'access_token' => VK_ACCESS_TOKEN,
        'v' => '5.37',
        );

    $result = file_get_contents($url, false, stream_context_create(array('http' =>
            array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($params)))));

    return $result;
}

// Удаляет комментарий к записи на стене.
function vkWallDeleteComment($owner_id, $comment_id)
{
    $url = 'https://api.vk.com/method/wall.deleteComment';
    $params = array(
        'owner_id' => $owner_id,
        'comment_id' => $comment_id,
        'access_token' => VK_ACCESS_TOKEN,
        'v' => '5.37',
        );

    $result = file_get_contents($url, false, stream_context_create(array('http' =>
            array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($params)))));

    return $result;
}

// Возвращает список комментариев к записи на стене.
function vkWallGetComments($owner_id, $post_id)
{
    $url = 'https://api.vk.com/method/wall.getComments';
    $params = array(
        'owner_id' => $owner_id,
        'post_id' => $post_id,
        'access_token' => VK_ACCESS_TOKEN,
        'v' => '5.37',
        );

    $result = file_get_contents($url, false, stream_context_create(array('http' =>
            array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($params)))));

    return $result;
}

// Удаляет тему в обсуждениях группы.
function vkBoardDeleteTopic($group_id, $topic_id)
{
    $url = 'https://api.vk.com/method/board.deleteTopic';
    $params = array(
        'group_id' => $group_id,
        'topic_id' => $topic_id,
        'access_token' => VK_ACCESS_TOKEN,
        'v' => '5.37',
        );

    $result = file_get_contents($url, false, stream_context_create(array('http' =>
            array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($params)))));

    return $result;
}

// Возвращает расширенную информацию о пользователях.
function vkUsersGet($user_ids)
{
    $url = 'https://api.vk.com/method/users.get';
    $params = array(
        'user_ids' => $user_ids,
        'v' => '5.37',
        );

    $result = file_get_contents($url, false, stream_context_create(array('http' =>
            array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($params)))));

    return $result;
}

// Удаляет сообщения в указанной теме.
function vkBoardDeleteComment($group_id, $topic_id, $comment_id)
{
    $url = 'https://api.vk.com/method/board.deleteComment';
    $params = array(
        'group_id' => $group_id,
        'topic_id' => $topic_id,
        'comment_id' => $comment_id,
        'access_token' => VK_ACCESS_TOKEN,
        'v' => '5.37',
        );

    $result = file_get_contents($url, false, stream_context_create(array('http' =>
            array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($params)))));

    return $result;
}

// Возвращает список сообщений в указанной теме.
function vkBoardGetComments($group_id, $topic_id, $offset = 0)
{
    $url = 'https://api.vk.com/method/board.getComments';
    $params = array(
        'group_id' => $group_id,
        'topic_id' => $topic_id,
        'offset' => $offset,
        'count' => 50,
        'sort' => 'desc',
        'v' => '5.37',
        );

    $result = file_get_contents($url, false, stream_context_create(array('http' =>
            array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($params)))));

    return $result;
}

// Получить список тем групы
function vkBoardGetTopics($group_id)
{
    $url = 'https://api.vk.com/method/board.getTopics';
    $params = array(
        'group_id' => $group_id,
        'v' => '5.37',
        );

    $result = file_get_contents($url, false, stream_context_create(array('http' =>
            array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($params)))));

    return $result;
}

// Отправка сообщения в личку
function vkMessagesSend($id, $message)
{
    $url = 'https://api.vk.com/method/messages.send';
    $params = array(
        'user_id' => $id,
        'message' => $message,
        'access_token' => VK_ACCESS_TOKEN,
        'v' => '5.37',
        );

    // В $result вернется id отправленного сообщения
    $result = file_get_contents($url, false, stream_context_create(array('http' =>
            array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($params)))));

    return $result;
}

// Удаляет запись со стены.
function vkWallDelete($owner_id, $post_id)
{
    $url = 'https://api.vk.com/method/wall.delete';
    $params = array(
        'owner_id' => $owner_id,
        'post_id' => $post_id,
        'access_token' => VK_ACCESS_TOKEN,
        'v' => '5.37',
        );

    $result = file_get_contents($url, false, stream_context_create(array('http' =>
            array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($params)))));

    return $result;
}

// Является ли пользователь участником сообщества.
function vkGroupsIsMember($group_id, $user_ids, $extended = 0)
{
    $url = 'https://api.vk.com/method/groups.isMember';
    $params = array(
        'group_id' => $group_id,
        'user_ids' => $user_ids,
        'extended' => $extended,
        'v' => '5.37',
        );

    $result = file_get_contents($url, false, stream_context_create(array('http' =>
            array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($params)))));

    return $result;
}

// Получить записи со стены
function vkWallGet($owner_id, $offset = 0)
{
    $url = 'https://api.vk.com/method/wall.get';
    $params = array(
        'owner_id' => $owner_id,
        'offset' => $offset,
        'count' => 50,
        'extended' => 1,
        'v' => '5.37',
        );

    $result = file_get_contents($url, false, stream_context_create(array('http' =>
            array(
            'method' => 'POST',
            'header' => 'Content-type: application/x-www-form-urlencoded',
            'content' => http_build_query($params)))));

    return $result;
}

class users_vk
{
    const MAX_TIME = 691200;

    private static $PDO = null;

    private static $ids = [];

    private static $users = [];

    private static $violators = [];

    private static $status = false;

    private function __construct()
    {
    }

    public static function init()
    {
        self::$PDO = new \PDO(PDO_DSN, DB_USERNAME, DB_PASSWORD, array(\PDO::
                ATTR_PERSISTENT => DB_PERSISTENCY));
        self::$PDO->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    public static function status($owner_id)
    {
        if (!isset(self::$violators[$owner_id])) {
            self::$violators[$owner_id] = [];
        }

        if (isset(self::$violators[$owner_id]) && !empty(self::$violators[$owner_id])) {
            return true;
        }

        return false;
    }

    public static function setId($id)
    {
        if (!isset(self::$users[$id]) && !in_array($id, self::$ids)) {
            self::$ids[] = $id;
        }
    }

    public static function process($owner_id)
    {
        for ($offset = 0; $offset < count(self::$ids); $offset = $offset + 100) {
            $ids = array_slice(self::$ids, $offset, 100);

            if (!empty($ids)) {
                foreach (self::getViolators($owner_id, $ids, COUNT_VIOLATION) as $violator) {
                    self::$violators[$owner_id][$violator['user_id']] = 1;
                }
            }

            $result = json_decode(vkGroupsIsMember($owner_id, $ids));

            $users_inf = [];
            $items = [];

            if ($result && is_object($result) && isset($result->response) && is_array($result->
                response)) {
                foreach ($result->response as $item) {
                    $items[$item->user_id] = $item;
                    if ($item->member == 1) {
                    } else {

                    }
                }
            }

            unset($result);

            foreach ($ids as $id) {
                if (isset($items[$id]) && isset($items[$id]->member) && $items[$id]->member == 1) {
                    self::$users[$id] = ['violation' => false];
                } else {
                    self::$users[$id] = ['violation' => true];
                    self::$violators[$owner_id][$id] = 1;
                    $users_inf[] = $item->user_id;
                }
            }

            if (!empty($users_inf)) {
                $result = json_decode(vkUsersGet($users_inf));

                if ($result && is_object($result) && isset($result->response) && is_array($result->
                    response)) {
                    foreach ($result->response as $item) {
                        if (!isset(self::$users[$item->id])) {
                            self::$users[$item->id] = ['violation' => null];
                        }

                        self::$users[$item->id]['inf'] = $item;
                    }
                }
            }
        }

        self::$ids = [];
    }

    public static function is($user_id, $group_id)
    {
        $stat = self::$PDO->prepare("SELECT COUNT(*) FROM `vk_violators` WHERE user_id=:user_id AND group_id=:group_id");

        if ($stat) {
            $stat->execute(['user_id' => $user_id, 'group_id' => $group_id]);
            $result = $stat->fetch(\PDO::FETCH_NUM);

            if ($result[0] > 0) {
                return true;
            } elseif ($result[0] == 0) {
                return false;
            }

        }

        return null;
    }

    private static function add($user_id, $group_id, $time_unix)
    {
        $stat = self::$PDO->prepare("INSERT INTO `vk_violators`(
                 user_id, 
                 group_id, 
                 count_violation, 
                 time_unix
             ) VALUES (
                 :user_id, 
                 :group_id, 
                 :count_violation, 
                 :time_unix
             )");

        if ($stat) {
            $stat->execute(['user_id' => $user_id, 'group_id' => $group_id,
                'count_violation' => 1, 'time_unix' => $time_unix]);
        }

        return 1;
    }

    public static function store($user_id, $group_id, $time_unix)
    {
        if (self::is($user_id, $group_id) === false) {
            return self::add($user_id, $group_id, $time_unix);
        } else {
            return self::increment($user_id, $group_id, $time_unix);
        }
    }

    private static function increment($user_id, $group_id, $time_unix)
    {
        $stat = self::$PDO->prepare("UPDATE `vk_violators`
             SET `count_violation` = `count_violation` + 1, 
                 `time_unix` = :time_unix
             WHERE `user_id` = :user_id AND `group_id` = :group_id");

        if ($stat) {
            $stat->execute(['user_id' => $user_id, 'group_id' => $group_id, 'time_unix' => $time_unix, ]);
        }

        $result = 1;

        $stat = self::$PDO->prepare("SELECT count_violation FROM `vk_violators`
             WHERE `user_id` = :user_id AND `group_id` = :group_id");

        if ($stat) {
            $stat->execute(['user_id' => $user_id, 'group_id' => $group_id, ]);

            $result = $stat->fetch(\PDO::FETCH_NUM);

            if (is_array($result) && !empty($result)) {
                $result = $result[0];
            }
        }

        return $result;
    }

    private static function getViolators($group_id, $ids, $max_violation)
    {
        $stat = self::$PDO->prepare("SELECT user_id, count_violation
             FROM `vk_violators`
             WHERE `group_id` = :group_id 
               AND count_violation > :max_violation 
               AND user_id IN (" . implode(',', array_map(function ($id)
        {
            return "'" . preg_replace("#[^0-9]#", '', $id) . "'"; }
        , $ids)) . ")
             ORDER BY time_unix DESC
             LIMIT 1000");

        if ($stat) {
            $stat->execute(['group_id' => $group_id, 'max_violation' => $max_violation, ]);

            $result = $stat->fetchAll(\PDO::FETCH_ASSOC);

            if (is_array($result)) {
                return $result;
            }
        }

        return [];
    }

    public static function getName($id)
    {
        if (isset(self::$users[$id]) && isset(self::$users[$id]['inf'])) {
            return ['first_name' => self::$users[$id]['inf']->first_name, 'last_name' =>
                self::$users[$id]['inf']->last_name, ];
        }

        return ['first_name' => '', 'last_name' => '', ];
    }

    public static function isViolator($user_id, $group_id)
    {
        if (isset(self::$violators[$group_id]) && isset(self::$violators[$group_id][$user_id])) {
            return self::$violators[$group_id][$user_id];
        }

        return null;
    }

    public static function delete($user_id, $group_id)
    {
        $stat = self::$PDO->prepare("DELETE FROM `vk_violators` WHERE user_id=:user_id AND group_id=:group_id");

        if ($stat) {
            $stat->execute(['user_id' => $user_id, 'group_id' => $group_id]);
        }
    }
}
