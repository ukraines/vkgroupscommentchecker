<?php

// Идеи
// Постинг рандомно от пользователя или от админа
// Выбор фоток согласно сезону. Если зима, то мы не можем постить фото с лета и наборот. Если сегодня скажем 2 декабря, то самая рання фотка в нашем случае должна быть из месяцев дек янв фев

set_time_limit(1200);

define('DB_PERSISTENCY', true); // Постоянное соединения
define('DB_SERVER', 'localhost');

define('DB_USERNAME', 'demo_repost');
define('DB_PASSWORD', 'WHrMLwSczE');
define('DB_DATABASE', 'demo_repost');

define('DB_CHARSET', 'utf8');
define('PDO_DSN', 'mysql:host=' . DB_SERVER . ';dbname=' . DB_DATABASE .
    ';charset=' . DB_CHARSET);
define('DB_TABLE_FLICKR', 'flickr_photo_post_vk');
define('DB_TABLE_FLICKR_PAGES', 'flickr_pages');

define('VK_ACCESS_TOKEN',
    'd8165f4ae644896e4cf55963728e6ce6099ea7fbe4a0fde51e73dd90fd1dad0f4b18652365713db288196');
define('FLIKR_API_KEY', 'c30a64b6c4d097ae6893fecd83242547');

define('FLIKR_NUMBER_PHOTO_PROCESS', 1); // Количество фото за один запуск скрипта.
define('FLIKR_MAX_PHOTO_FOR_HOURS', 25); // Максимальное количество фото за ? часа.
define('FLIKR_CONTROL_HOURS', 24); // Количество часов для контроля максимального количества.
define('FLIKR_MAX_PAGES', 20); // Максимальное количество запрашиваемых страниц при поиске фото. 1 страница 100 фото.

/**
 * Возможные значения: (высота фото)
 * 'Original' : Оригинальное фото (Может быть большим)
 * 'Square' : 75px
 * 'Large Square' : 150px
 * 'Thumbnail' : 100px
 * 'Small' : 240px
 * 'Small 320' : 320px
 * 'Medium' : 500px
 * 'Medium 640' : 640px
 * 'Medium 800' : 800px
 * 'Large' : 1024px
 * 'Large 1600' : 1600px
 * 'Large 2048' : 2048px
 * 
 * Если указанного размера не найдено будет попытка найти размер меньше вверх по списку выше. 
 * Последний искомый размер Original.
 * 
 * Принимаются только форматы jpg, gif, png. Ограничения Вконтакте.
 * Если фото будет другого формата оно будет отмечено но не опубликовано.
 */
define('FLIKR_PHOTO_SIZE', 'Medium');

/**
 * Ключ масива: это идентификатор групы источника FLICKR.
 * Значения: массив целевых груп. 
 * 
 * Идентификаторы груп указывать со знаком -
 * 
 * id: обязательный. Идентификатор групы Вконтакте.
 * type: обязательный. people - пользовать, group - група
 * ids: Идентификаторы груп Вконтакте.
 * 
 */

$flickr_ids = ['82173652@N02' => ['type' => 'people', 'ids' => ['-32439659', ]], ];

$message_post = "Велтон Парк Новая Сходня Вконтакте, ставь лайк \n\n #ретро #велтонпарк #велтонпаркноваясходня #сходня #химки #стройка %tags% ";
// $message_post = "%description% \n\n Дата добавления flickr %dateadded% \n\n Источник %sender% \n\n Теги %tags% ";
$message_photo = "Велтон Парк Новая Сходня Вк: https://vk.com/weltonpark\n\nИнстаграм Велтон Парк Новая Сходня https://www.instagram.com/weltonpark \n\n Дата съемки:  %dateadded% ";


srand(make_seed());

db_flickr::init();

Flickr::$api_key = FLIKR_API_KEY;

$flickr = new Flickr($flickr_ids, FLIKR_NUMBER_PHOTO_PROCESS,
    FLIKR_MAX_PHOTO_FOR_HOURS, FLIKR_CONTROL_HOURS, FLIKR_PHOTO_SIZE,
    FLIKR_MAX_PAGES, $message_post, $message_photo);

$flickr->init();

Log::render();

class Flickr
{
    public static $api_key;

    private $groups;

    private $count_photo_process;
    private $max_photo;
    private $control_hour;
    private $photo_size;

    private $max_pages;

    private $message_post_vk;
    private $message_photo_vk;

    public static $size = [1 => 'Original', 2 => 'Square', 3 => 'Large Square', 4 =>
        'Thumbnail', 5 => 'Small', 6 => 'Small 320', 7 => 'Medium', 8 => 'Medium 640', 9 =>
        'Medium 800', 10 => 'Large', 11 => 'Large 1600', 12 => 'Large 2048', ];

    public function __construct($groups, $count_photo_process, $max_photo, $control_hour,
        $photo_size, $max_pages, $message_post_vk, $message_photo_vk)
    {
        $this->groups = $groups;

        $this->count_photo_process = $count_photo_process;
        $this->max_photo = $max_photo;
        $this->control_hour = $control_hour;
        $this->photo_size = $photo_size;
        $this->max_pages = $max_pages;
        $this->message_post_vk = $message_post_vk;
        $this->message_photo_vk = $message_photo_vk;
    }

    public function init()
    {
        $this->process();
    }

    private function process()
    {
        foreach ($this->groups as $group_id => $group) {
            Log::add('Flickr. ID: ' . $group_id);

            $vk_groups = $group['ids'];

            foreach ($vk_groups as $vk_group) {
                $count_post = db_flickr::count($group_id, $vk_group, $this->control_hour * 3600);

                if (!is_null($count_post) && $count_post < $this->max_photo) {
                    $photos = $this->search($count_post, $group_id, $group['type'], $vk_group, $this->
                        count_photo_process);

                    if (is_array($photos) && !empty($photos)) {
                        Log::add($group_id . ': найдено Фото: ' . count($photos));
                        $this->publish($photos, $group_id, $vk_group);
                    } else {
                        Log::add($group_id . ' Фото не найдено');
                    }
                } else {
                    Log::add($group_id . ' Достигнуто максимальное количество');
                }
            }
        }
    }

    private static function nextSize($size)
    {
        $key_size = null;

        foreach (self::$size as $key => $value) {
            if ($size == $value) {
                $key_size == $key--;

                break;
            }
        }

        if ($key_size && $key_size > 0 && isset(self::$size[$key_size])) {
            return self::$size[$key_size];
        }

        return null;
    }

    private function publish($photos, $flickr_group_id, $vk_group)
    {
        if (!is_dir(__dir__ . '/tmp')) {
            mkdir(__dir__ . '/tmp', 0777, true);
        }

        foreach ($photos as $id => $photo) {
            if ($photo['url']) {
                $pathinfo = pathinfo($photo['url']);

                if (isset($pathinfo['dirname']) && isset($pathinfo['basename']) && isset($pathinfo['extension']) &&
                    isset($pathinfo['filename'])) {
                    if (file_put_contents(__dir__ . '/tmp/' . $pathinfo['basename'],
                        file_get_contents($photo['url']))) {
                        $item = new \stdClass();

                        $item->key = $flickr_group_id . '_' . $photo['id'];

                        $text = $this->message_post_vk;
                        if (isset($photo['inf']['description']) && is_array($photo['inf']['description'])) {
                            $description = $photo['inf']['description'];
                            if (is_array($description) && isset($description['_content'])) {
                                $text = str_replace('%description%', $description['_content'], $text);
                            }
                        }
                        $url = null;
                        if (isset($photo['inf']['urls']) && is_array($photo['inf']['urls']) && isset($photo['inf']['urls']['url']) &&
                            is_array($photo['inf']['urls']['url']) && !empty($photo['inf']['urls']['url'])) {
                            foreach ($photo['inf']['urls']['url'] as $url) {
                                if (is_array($url) && isset($url['_content']) && $url['type'] == 'photopage') {
                                    $url = $url['_content'];
                                    break;
                                }
                            }
                        }
                        if (!$url) {
                            if ($photo['type_owner'] == 'people') {
                                $url = 'https://www.flickr.com/photos/' . $flickr_group_id;
                            } else {
                                $url = 'https://www.flickr.com/groups/' . $flickr_group_id;
                            }
                        }
                        $text = str_replace('%sender%', $url, $text);
                        $text = str_replace('%dateadded%', date("d/m/Y H:i", $photo['dateadded']), $text);
                        $vk_tags = [];
                        if (isset($photo['inf']['tags']) && is_array($photo['inf']['tags']) && isset($photo['inf']['tags']['tag'])) {
                            $tags = $photo['inf']['tags']['tag'];

                            if (is_array($tags) && !empty($tags)) {
                                foreach ($tags as $tag) {
                                    if (is_array($tag) && isset($tag['_content'])) {
                                        $vk_tags[] = $tag['_content'];
                                    }
                                }
                            }
                        }
                        $text = str_replace('%tags%', implode(' ', array_map(function ($value)
                        {
                            return '#' . $value; }
                        , $vk_tags)), $text);
                        $text = preg_replace("#%.*?%#", '', $text);
                        $item->text = $text;

                        $text = $this->message_photo_vk;
                        $text = str_replace('%dateadded%', date("d/m/Y H:i", $photo['dateadded']), $text);
                        if (isset($photo['inf']['title']) && is_array($photo['inf']['title'])) {
                            $title = $photo['inf']['title'];
                            if (is_array($title) && isset($title['_content'])) {
                                $text = str_replace('%title%', $title['_content'], $text);
                            }
                        }
                        $text = preg_replace("#%.*?%#", '', $text);
                        $item->caption_photo = $text;

                        $post_vk = VkPost::exists($item);
                        if ($post_vk->addPhoto($vk_group, __dir__ . '/tmp/' . $pathinfo['basename'], $pathinfo['basename'])) {
                            $post_vk->wallPost($vk_group);
                        }

                        unlink(__dir__ . '/tmp/' . $pathinfo['basename']);
                    }
                }
            }
        }
    }

    private function checkPhoto($photo, $flickr_group_id, $vk_group_id)
    {
        if ($this->photo_size && in_array($this->photo_size, self::$size)) {
            $size_result = $this->photo_size;
        } else {
            $size_result = 'Original';
        }

        $inf = self::HTTP(['method' => 'flickr.photos.getInfo', 'photo_id' => $photo['id'], ]);

        if ($inf && isset($inf['photo'])) {
            if (!isset($photo['dateadded'])) {
                $photo['dateadded'] = $inf['photo']['dateuploaded'];
            }

            $sizes = self::HTTP(['method' => 'flickr.photos.getSizes', 'photo_id' => $photo['id'], ]);

            if ($sizes && isset($sizes['sizes']) && isset($sizes['sizes']['size'])) {
                $url = null;

                while ($size_result && !$url) {
                    foreach ($sizes['sizes']['size'] as $size) {
                        if ($size['label'] == $size_result) {
                            $pathinfo = pathinfo($size['source']);
                            if (isset($pathinfo['dirname']) && isset($pathinfo['basename']) && isset($pathinfo['extension']) &&
                                isset($pathinfo['filename']) && ($pathinfo['extension'] == 'jpg' || $pathinfo['extension'] ==
                                'jpeg' || $pathinfo['extension'] == 'png' || $pathinfo['extension'] == 'gif')) {
                                $url = $size['source'];
                                break;
                            }
                        }
                    }

                    $size_result = self::nextSize($size_result);
                }

                if ($url) {
                    db_flickr::check($flickr_group_id, $photo['id'], $vk_group_id, $photo['dateadded'],
                        time(), 1);
                    return array_merge($photo, ['size' => $sizes['sizes']['size'], 'inf' => $inf['photo'],
                        'url' => $url]);
                } else {
                    db_flickr::check($flickr_group_id, $photo['id'], $vk_group_id, $photo['dateadded'],
                        time(), 0);
                }
            }
        }

        return null;
    }

    private function search($count, $flickr_group_id, $type, $vk_group_id, $number)
    {
        $photos = [];

        $status = true;

        $page = db_flickr::getPage($flickr_group_id, $vk_group_id);
        
        $max_page = $page + $this->max_pages;

        if ($type != 'people') {
            $type = 'group';
        }
        
        while ($status && $number > 0 && $page <= $max_page) {
            Log::add($flickr_group_id . ' Поиск на странице: ' . $page);
            
            $status = false;

            if ($type == 'people') {
                $rsp = self::HTTP(['method' => 'flickr.photos.search', 'sort' =>
                    'date-posted-asc', 'user_id' => $flickr_group_id, 'page' => $page, ]);
            } else {
                $rsp = self::HTTP(['method' => 'flickr.groups.pools.getPhotos', 'group_id' => $flickr_group_id,
                    'page' => $page, ]);
            }

            if ($rsp && isset($rsp['photos'])) {
                if (isset($rsp['photos']['photo']) && is_array($rsp['photos']['photo'])) {

                    $ids = [];

                    foreach ($rsp['photos']['photo'] as $photo) {
                        $ids[] = $photo['id'];
                    }

                    if (!empty($ids)) {
                        $db_rows = db_flickr::getForIds($ids, $vk_group_id);

                        if (is_array($db_rows)) {
                            foreach ($rsp['photos']['photo'] as $photo) {
                                if (!isset($db_rows[$photo['id']]) && !isset($photos[$photo['id']]) && count($photos) <
                                    $number) {
                                    $photo = $this->checkPhoto($photo, $flickr_group_id, $vk_group_id);
                                    if ($photo) {
                                        $photo['type_owner'] = $type;

                                        $photos[$photo['id']] = $photo;
                                    }
                                }

                                if (count($photos) == $number) {
                                    db_flickr::setPage($page, $flickr_group_id, $vk_group_id);
                                    return $photos;
                                }

                            }

                            if (count($photos) < $number) {
                                if ($rsp['photos']['pages'] > $page) {
                                    $page++;
                                    $status = true;
                                }
                            }
                        }
                    }
                }
            } else {
                Log::add("Error" . $flickr_group_id . ' Ответ не корректный');
            }

            db_flickr::setPage($page, $flickr_group_id, $vk_group_id);
        }

        return $photos;
    }

    public static function HTTP($params)
    {
        if (!is_array($params)) {
            return false;
        }

        if (!isset($params['api_key'])) {
            if (self::$api_key) {
                $params['api_key'] = self::$api_key;
            } else {
                return false;
            }
        }

        if (!isset($params['format'])) {
            $params['format'] = 'php_serial';
        }

        $encoded_params = [];

        foreach ($params as $k => $v) {
            $encoded_params[] = urlencode($k) . '=' . urlencode($v);
        }

        $url = "https://api.flickr.com/services/rest/?" . implode('&', $encoded_params);

        $rsp = file_get_contents($url);

        if ($params['format'] == 'php_serial') {
            $rsp_obj = unserialize($rsp);
            if (is_array($rsp_obj) && isset($rsp_obj['stat']) && $rsp_obj['stat'] == 'ok') {

            } else {
                $rsp_obj = null;
            }
        } elseif ($params['format'] == 'json') {
            $rsp_obj = json_decode(trim(str_replace('jsonFlickrApi(', '', $rsp), '()'));
        } else {
            $rsp_obj = $rsp;
        }


        if ($rsp_obj) {
            return $rsp_obj;
        } else {
            return null;
        }
    }
}

class VkPost
{
    private static $upload_uls = [];

    private static $posts = [];

    public $id = null;
    public $from_id = null;
    public $owner_id = null;
    public $date = null;
    public $type = null;
    public $text = null;
    public $caption_photo = null;
    public $count_comments = null;
    public $count_likes = null;
    public $count_reposts = null;

    public $attachments = [];

    private function __construct(&$item)
    {
        if (isset($item->id)) {
            $this->id = $item->id;
        }
        if (isset($item->from_id)) {
            $this->from_id = $item->from_id;
        }
        if (isset($item->owner_id)) {
            $this->owner_id = $item->owner_id;
        }
        if (isset($item->date)) {
            $this->date = $item->date;
        }
        if (isset($item->post_type)) {
            $this->type = $item->post_type;
        }
        if (isset($item->caption_photo)) {
            $this->text = $item->text;
        }
        if (isset($item->caption_photo)) {
            $this->caption_photo = $item->caption_photo;
        }
        if (isset($item->comments) && isset($item->comments->count)) {
            $this->count_comments = $item->comments->count;
        }
        if (isset($item->likes) && isset($item->likes->count)) {
            $this->count_likes = $item->likes->count;
        }
        if (isset($item->reposts) && isset($item->reposts->count)) {
            $this->count_reposts = $item->reposts->count;
        }
        if (isset($item->attachments) && is_array($item->attachments)) {
            foreach ($item->attachments as $attach) {
                if (is_object($attach) && isset($attach->type)) {
                    $type = $attach->type;

                    if (isset($attach->$type)) {
                        $object = $attach->$type;
                        $this->attachments[] = $type . $object->owner_id . '_' . $object->id;
                    }
                }
            }
        }
    }

    public function addPhoto($group_id, $file_path, $basename)
    {
        if (file_exists($file_path)) {
            if ($obj = $this->uploadFile($group_id, $file_path, $basename)) {
                if ($photo = $this->savePhoto($group_id, $obj)) {
                    Log::add($group_id . ': файл успешно сохранен на сервере');
                    $this->attachments[] = $photo;

                    return true;
                } else {
                    Log::add('Error: ' . $group_id . ' ошибка сохранения загруженого фото');
                }
            } else {
                Log::add('Error: ' . $group_id . ' ошибка загрузки фото на сервер');
            }
        } else {
            Log::add('Error: ' . $group_id . ' Файл не найден -' . $basename);
        }

        return false;
    }

    private function uploadFile($group_id, $file_path, $basename)
    {
        $url = $this->getUploadUrl($group_id);

        if ($url) {
            $post_params = ['photo' => curl_file_create($file_path, null, $basename), ];

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post_params);
            $response = curl_exec($ch);
            curl_close($ch);

            $obj = json_decode($response);

            if (is_object($obj)) {
                if (isset($obj->server) && isset($obj->photo) && isset($obj->hash)) {
                    if ($obj->photo && $obj->photo != '[]') {
                        $obj->photo = stripslashes($obj->photo);

                        return $obj;
                    }
                }
            }
        }

        return false;
    }

    private function savePhoto($group_id, $obj)
    {
        $url = 'https://api.vk.com/method/photos.saveWallPhoto';
        $params = array(
            'group_id' => ltrim($group_id, '-'),
            'photo' => $obj->photo,
            'server' => $obj->server,
            'hash' => $obj->hash,
            'caption' => $this->caption_photo,
            'access_token' => VK_ACCESS_TOKEN,
            'v' => '5.37',
            );

        $result = file_get_contents($url, false, stream_context_create(array('http' =>
                array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)))));

        $obj = json_decode($result);

        if (is_object($obj)) {
            if (isset($obj->response) && is_array($obj->response) && isset($obj->response[0]) &&
                is_object($obj->response[0]) && isset($obj->response[0]->id) && isset($obj->
                response[0]->owner_id)) {
                return 'photo' . $obj->response[0]->owner_id . '_' . $obj->response[0]->id;
            }
        }

        return false;
    }

    private function getUploadUrl($group_id)
    {
        if (isset(self::$upload_uls[$group_id])) {
            return self::$upload_uls[$group_id];
        }

        $url = 'https://api.vk.com/method/photos.getWallUploadServer';
        $params = array(
            'group_id' => ltrim($group_id, '-'),
            'access_token' => VK_ACCESS_TOKEN,
            'v' => '5.37',
            );

        $result = file_get_contents($url, false, stream_context_create(array('http' =>
                array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)))));

        $obj = json_decode($result);

        if (is_object($obj)) {
            if (isset($obj->response) && is_object($obj->response) && isset($obj->response->
                upload_url) && $obj->response->upload_url) {
                self::$upload_uls[$group_id] = $obj->response->upload_url;
            }
        }

        if (isset(self::$upload_uls[$group_id])) {
            return self::$upload_uls[$group_id];
        }

        return null;
    }

    public static function exists(&$item)
    {
        if (isset($item->key)) {
            $key = $item->key;
        } else {
            $key = $item->owner_id . '_' . $item->id;
        }

        if (!isset(self::$posts[$key])) {
            self::$posts[$key] = new VkPost($item);
        }

        return self::$posts[$key];
    }

    public function wallPost($owner)
    {

        $url = 'https://api.vk.com/method/wall.post';
        $params = array(
            'owner_id' => $owner,
            'from_group' => rand(0, 1),
            'message' => $this->text,
            'attachments' => implode(',', $this->attachments),
            'group_id' => ltrim($owner, '-'),
            'access_token' => VK_ACCESS_TOKEN,
            'v' => '5.37',
            );

        $result = json_decode(file_get_contents($url, false, stream_context_create(array
            ('http' => array(
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params))))));

        if (is_object($result) && isset($result->response) && isset($result->response->
            post_id) && $result->response->post_id > 0) {
            Log::add($owner . ': пост успешно опубликован');
            return true;
        } else {
            Log::add('Error: ' . $owner . ': ошибка публикации поста');
        }
    }
}

class db_flickr
{
    private static $PDO = null;

    private function __construct()
    {
    }

    public static function init()
    {
        self::$PDO = new \PDO(PDO_DSN, DB_USERNAME, DB_PASSWORD, array(\PDO::
                ATTR_PERSISTENT => DB_PERSISTENCY));

        // self::$PDO->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    public static function count($flickr_group_id, $vk_group_id, $seconds)
    {
        $stat = self::$PDO->prepare("SELECT COUNT(*) as count FROM `" . DB_TABLE_FLICKR .
            "` WHERE `flickr_group_id`=:flickr_group_id AND `vk_group_id`=:vk_group_id AND `time_unix`>:time_unix");

        if ($stat) {
            $stat->execute(['flickr_group_id' => $flickr_group_id, 'vk_group_id' => $vk_group_id,
                'time_unix' => time() - $seconds]);
            $result = $stat->fetch(\PDO::FETCH_ASSOC);

            if (isset($result['count'])) {
                return $result['count'];
            }
        }

        return null;
    }

    public static function getForIds($ids, $vk_group_id)
    {
        $stat = self::$PDO->prepare("SELECT flickr_photo_id FROM `" . DB_TABLE_FLICKR .
            "` WHERE `vk_group_id`=:vk_group_id AND flickr_photo_id IN (" . implode(',',
            array_map(function ($id)
        {
            return "'" . preg_replace("#[^0-9]#", '', $id) . "'"; }
        , $ids)) . ")");

        if ($stat) {
            $stat->execute(['vk_group_id' => $vk_group_id, ]);

            $result = $stat->fetchAll(\PDO::FETCH_ASSOC);

            if (is_array($result)) {
                $rows = [];

                foreach ($result as $row) {
                    $rows[$row['flickr_photo_id']] = $row;
                }

                return $rows;
            }
        }

        return null;
    }

    public static function check($flickr_group_id, $flickr_photo_id, $vk_group_id, $flickr_dateadded,
        $time_unix, $status)
    {
        $stat = self::$PDO->prepare("INSERT
            INTO
              `flickr_photo_post_vk`(
                `flickr_group_id`,
                `flickr_photo_id`,
                `vk_group_id`,
                `flickr_dateadded`,
                `time_unix`,
                `status`
              )
            VALUES(
              :flickr_group_id,
              :flickr_photo_id,
              :vk_group_id,
              :flickr_dateadded,
              :time_unix,
              :status
            )");

        if ($stat) {
            $stat->execute(['flickr_group_id' => $flickr_group_id, 'flickr_photo_id' => $flickr_photo_id,
                'vk_group_id' => $vk_group_id, 'flickr_dateadded' => $flickr_dateadded,
                'time_unix' => $time_unix, 'status' => $status, ]);
        }
    }

    public static function add($key_post, $date)
    {
        $stat = self::$PDO->prepare("INSERT INTO `posts`(key_post, time_post) VALUES (:key_post, :time_post);");

        if ($stat) {
            $stat->execute(['key_post' => $key_post, 'time_post' => $date]);
        }
    }

    public static function setPage($number, $flickr_group_id, $vk_group_id)
    {
        $stat = self::$PDO->prepare("SELECT COUNT(*) as count FROM `" .
            DB_TABLE_FLICKR_PAGES .
            "` WHERE `flickr_group_id`=:flickr_group_id AND `vk_group_id`=:vk_group_id LIMIT 1");

        if ($stat) {
            $stat->execute(['flickr_group_id' => $flickr_group_id, 'vk_group_id' => $vk_group_id, ]);
            $result = $stat->fetch(\PDO::FETCH_ASSOC);
            if (isset($result['count']) && $result['count'] == 0) {

                $stat = self::$PDO->prepare("INSERT
                           INTO
                             `" . DB_TABLE_FLICKR_PAGES . "`(
                               `flickr_group_id`,
                               `vk_group_id`,
                               `page`
                             )
                       VALUES(
                           :flickr_group_id, 
                           :vk_group_id, 
                           :page
                           )");

                if ($stat) {
                    $stat->execute(['flickr_group_id' => $flickr_group_id, 'vk_group_id' => $vk_group_id,
                        'page' => $number, ]);
                }
            } else {
                $stat = self::$PDO->prepare("UPDATE
                           `" . DB_TABLE_FLICKR_PAGES . "`
                         SET
                           `page`=:page
                       WHERE 
                         `flickr_group_id`=:flickr_group_id 
                       AND 
                         `vk_group_id`=:vk_group_id");

                if ($stat) {
                    $stat->execute(['flickr_group_id' => $flickr_group_id, 'vk_group_id' => $vk_group_id,
                        'page' => $number, ]);
                }
            }
        }
    }

    public static function getPage($flickr_group_id, $vk_group_id)
    {
        $stat = self::$PDO->prepare("SELECT page FROM `" . DB_TABLE_FLICKR_PAGES .
            "` WHERE `flickr_group_id`=:flickr_group_id AND  `vk_group_id`=:vk_group_id LIMIT 1");

        if ($stat) {
            $stat->execute(['flickr_group_id' => $flickr_group_id, 'vk_group_id' => $vk_group_id, ]);

            $result = $stat->fetch(\PDO::FETCH_ASSOC);

            if (isset($result['page'])) {
                $number = intval($result['page']);
                if ($number > 0) {
                    return $number;
                }
            }
        }

        return 1;
    }
}

class Log
{
    private static $stack = [];

    private function __construct()
    {
    }

    public static function add($event)
    {
        self::$stack[] = $event;
    }

    public static function render()
    {
        echo implode("<br />\n", self::$stack);
    }
}

function make_seed()
{
    list($usec, $sec) = explode(' ', microtime());
    return (float)$sec + ((float)$usec * 100000);
}
