CREATE TABLE `vk_violators` (
  `user_id` VARCHAR(40) NOT NULL,
  `group_id` VARCHAR(40) NOT NULL,
  `count_violation` INT(2) NOT NULL DEFAULT 0,
  `time_unix` INT(11)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `vk_violators`
  ADD UNIQUE KEY `user_group_key` (`user_id`, `group_id`);