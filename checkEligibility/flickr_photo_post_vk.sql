CREATE TABLE `flickr_photo_post_vk` (
  `flickr_group_id` VARCHAR(40) NOT NULL,
  `flickr_photo_id` VARCHAR(40) NOT NULL,
  `vk_group_id` VARCHAR(40) NOT NULL,
  `flickr_dateadded` INT(11),
  `time_unix` INT(11),
  `status` INT(1) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `flickr_photo_post_vk`
  ADD UNIQUE KEY `flickr_photo_vk_post` (`flickr_group_id`, `flickr_photo_id`, `vk_group_id`);
  
CREATE TABLE `flickr_pages` (
  `flickr_group_id` VARCHAR(40) NOT NULL,
  `vk_group_id` VARCHAR(40) NOT NULL,
  `page` INT(11)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `flickr_pages`
  ADD UNIQUE KEY `flickr_page` (`flickr_group_id`, `vk_group_id`);