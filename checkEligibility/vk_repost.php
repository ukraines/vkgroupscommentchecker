<?php 
set_time_limit(1200);

define('DB_PERSISTENCY', true); // Постоянное соединения
define('DB_SERVER', 'localhost');
define('DB_USERNAME', 'login');
define('DB_PASSWORD', 'parol');
define('DB_DATABASE', 'vkgroups');
define('DB_CHARSET', 'utf8');
define('PDO_DSN', 'mysql:host=' . DB_SERVER . ';dbname=' . DB_DATABASE . ';charset=' . DB_CHARSET);

define('VK_ACCESS_TOKEN', 'b648a68370d9a8bf41d96255de3dcd55ba118300100d7da1602d9a135bbc6fa26e2d46aaaaba87a981f30');

/** 
 * Ключ масива: это идентификатор групы источника.
 * Значения: массив целевых груп. 
 * 
 * Идентификаторы груп указывать со знаком -
 * 
 * id: обязательный.
 * act: не обязательный. 1 - только репост, 2 - только пост, 3 - сперва репост и в случае неудачи пост. По умолчанию 3.
 * from_group: не обязательный. 0 - от имени пользователя, 1 - от имени групы. По умолчанию 0.
 */
$owners_from_to = [
    '-32439659' => [
        ['id' => '-127185744', 'act' => 3, 'from_group' => 1],
    ],
];

$markers = [
    '#горячаявода',
];

db_exists::init();
db_exists::clear();

$vk_repost = new vkRepost($owners_from_to, $markers);

$vk_repost->process();

class vkRepost {
    const MAX_TIME = 604800;
    
    private $owners = [];
    private $markers = [];
    
    private $repost_message = "репост";
    
    public function __construct($owners_from_to, $markers){
        $this->owners = $owners_from_to;
        $this->markers = $markers;
    }
    
    public function process(){
        foreach($this->owners as $owner_id_from => $owners){
            $items = $this->vkWallGet($owner_id_from, 0, 100);
            
            foreach($items as $item){
                foreach($this->markers as $marker){
                    if($item->date > time()-self::MAX_TIME && stripos($item->text, $marker)!==false){
                        foreach($owners as $owner_to){
                            if(!is_array($owner_to)){
                                $owner_to = ['id' => $owner_to];
                            }
                            
                            if(!isset($owner_to['act'])){
                                $owner_to ['act'] = 3;
                            }
                            if(!isset($owner_to['from_group'])){
                                $owner_to ['from_group'] = 0;
                            }
                            
                            $owner_to['owner_id_from'] = $owner_id_from;
                            
                            $post = vkPost::exists($item);
                            
                            if(isset($owner_to['id']) && $post->isDb($owner_to['id'])===false){
                                if(($owner_to['act'] == 1 || $owner_to['act'] == 3) && stripos($post->from_id, '-') === 0){
                                    $this->vkWallRepost($owner_to, $post);
                                } else {
                                    $post->wallPost($owner_to);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    private function vkWallGet($owner_id, $offset, $count){
        $url = 'https://api.vk.com/method/wall.get';
        $params = array(
            'owner_id' => $owner_id,  
            'offset' => $offset,  
            'count' => $count,    
            'fields' => 'id,first_name,last_name',   
            'v' => '5.37', 
        );

        $result = json_decode(file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        ))));
        
        if(is_object($result) && isset($result->response) && isset($result->response->count) && $result->response->count > 0){
            return $result->response->items;
        }
        
        return [];
    }
    
    private function vkWallRepost($owner, &$post){
        $url = 'https://api.vk.com/method/wall.repost';
        $params = array(
            'object' => 'wall'.$post->owner_id.'_'.$post->id,  
            'message' => $this->repost_message,  
            'group_id' => ltrim($owner['id'],'-'),    
            'mark_as_ads' => 0,   
            'access_token' => VK_ACCESS_TOKEN, 
            'v' => '5.37', 
        );

        $result = json_decode(file_get_contents($url, false, stream_context_create(array(
            'http' => array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => http_build_query($params)
            )
        ))));
        
        if(is_object($result)){
            if(isset($result->response) && isset($result->response->success) && $result->response->success==1){
                $post->saveDb($owner['id']);
            } elseif (isset($result->error) && isset($result->error->error_code) && $result->error->error_code==15) {
                $post->wallPost($owner);
            }
        }
    }
}

class vkPost {
    private static $posts = [];
    
    public $id = null;
    public $from_id = null;
    public $owner_id = null;
    public $date = null;
    public $type = null;
    public $text = null;
    public $count_comments = null;
    public $count_likes = null;
    public $count_reposts = null;
    
    public $attachments = [];
    
    private function __construct(&$item) { 
        if(isset($item->id)){
            $this->id = $item->id;
        }
        if(isset($item->from_id)){
            $this->from_id = $item->from_id;
        }
        if(isset($item->owner_id)){
            $this->owner_id = $item->owner_id;
        }
        if(isset($item->date)){
            $this->date = $item->date;
        }
        if(isset($item->post_type)){
            $this->type = $item->post_type;
        }
        if(isset($item->text)){
            $this->text = $item->text;
        }
        if(isset($item->comments)&&isset($item->comments->count)){
            $this->count_comments = $item->comments->count;
        }
        if(isset($item->likes)&&isset($item->likes->count)){
            $this->count_likes = $item->likes->count;
        }
        if(isset($item->reposts)&&isset($item->reposts->count)){
            $this->count_reposts = $item->reposts->count;
        }
        if(isset($item->attachments) && is_array($item->attachments)){
            foreach($item->attachments as $attach){
                if(is_object($attach) && isset($attach->type)){
                    $type = $attach->type;
                    
                    if(isset($attach->$type)){
                        $object = $attach->$type;
                        $this->attachments [] = $type . $object->owner_id . '_' . $object->id;
                    }
                }
            }
        }
    }
    
    public static function exists(&$item) {
        $key = $item->owner_id . '_' . $item->id;
        
        if (!isset(self::$posts[$key])) {
            self::$posts[$key] = new vkPost($item);
        }
        
        return self::$posts[$key];
    } 
    
    public function wallPost($owner){
        if($owner['act']==2||$owner['act']==3){
            $url = 'https://api.vk.com/method/wall.post';
            $params = array(
                'owner_id' => $owner['id'],  
                'from_group' => $owner['from_group'],  
                'message' => $this->text."\nИсточник: https://vk.com/club".ltrim($owner['owner_id_from'], '-'),  
                'attachments' => implode(',', $this->attachments), 
                'group_id' => ltrim($owner['id'],'-'),     
                'access_token' => VK_ACCESS_TOKEN,  
                'v' => '5.37', 
            );
            
            $result = json_decode(file_get_contents($url, false, stream_context_create(array(
                'http' => array(
                    'method'  => 'POST',
                    'header'  => 'Content-type: application/x-www-form-urlencoded',
                    'content' => http_build_query($params)
                )
            ))));
                
            if(is_object($result) && isset($result->response) && isset($result->response->post_id) && $result->response->post_id > 0){
                $this->saveDb($owner['id']);
            }
        }
    }
    
    public function isDb($owner_id){
        return db_exists::is($owner_id.'_'.$this->owner_id.'_'.$this->id);
    }
    
    public function saveDb($owner_id){
        return db_exists::add($owner_id.'_'.$this->owner_id.'_'.$this->id, $this->date);
    }
}

class db_exists {
    const DSN = 'sqlite:./posts.db';
    const MAX_TIME = 691200;
    
    private static $PDO = null;
    
    private function __construct(){}
    
    public static function init(){
        self::$PDO = new \PDO( PDO_DSN, DB_USERNAME, DB_PASSWORD, array(\PDO::ATTR_PERSISTENT => DB_PERSISTENCY) );
        
        $stat = self::$PDO->prepare("CREATE TABLE IF NOT EXISTS `posts`(key_post VARCHAR(60), time_post INT(11));");
        
        $stat->execute();
    }
    
    public static function is($key_post){
        $stat = self::$PDO->prepare("SELECT COUNT(*) FROM `posts` WHERE key_post=:key_post");
        
        if($stat){
            $stat->execute(['key_post' => $key_post]);
            $result = $stat->fetch(\PDO::FETCH_NUM);
            
            if($result[0] > 0){
                return true;
            } elseif($result[0] == 0){
                return false;
            }
                
        }
        
        return null;
    }
    
    public static function add($key_post, $date){
        $stat = self::$PDO->prepare("INSERT INTO `posts`(key_post, time_post) VALUES (:key_post, :time_post);");
        
        if($stat){
            $stat->execute(['key_post' => $key_post, 'time_post' => $date]);
        }
    }
    
    public static function delete($key_post){
        $stat = self::$PDO->prepare("DELETE FROM `posts` WHERE key_post=:key_post;");
        
        if($stat){
            $stat->execute(['key_post' => $key_post]);
        }
    }
    
    public static function clear(){
        $stat = self::$PDO->prepare("DELETE FROM `posts` WHERE time_post<:max_time;");
        
        if($stat){
            $stat->execute(['max_time' => time()-self::MAX_TIME]);
        }
    }
}