﻿<?php

function request_get($url,$post = 0){
//задержка 0.35сек чтобы больше 3 запросов в сек небыло
usleep(350000);
$headers = array(		
			'Accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
			'Accept-Encoding:gzip, deflate, sdch',
			'Accept-Language:ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
			'Cache-Control:max-age=0',
			'Connection:keep-alive',
			'Host:api.vk.com',
			'Upgrade-Insecure-Requests:1',
			'User-Agent:Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36'

		); 

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url ); 
  curl_setopt($ch, CURLOPT_HEADER, 0); 
  curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
  curl_setopt($ch, CURLOPT_ENCODING, '');
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_COOKIEJAR, dirname(__FILE__).'/cookie.txt'); 
  curl_setopt($ch, CURLOPT_COOKIEFILE,  dirname(__FILE__).'/cookie.txt');
  curl_setopt($ch, CURLOPT_POST, $post!==0 ); 
  if($post)
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
  $data = curl_exec($ch);

  curl_close($ch);

  return $data;
}


function rezult($mas,$word)
{         
        
        $html = '';
        $html .= '<table class="pure-table pure-table-horizontal"> <tr><th>User</th> <th>Content</th> <th>Source</th></tr>';

        foreach ($mas as $key => $arr) {
	if($word==''){
		$html .= '<tr>'.
				'<td >'.$arr['uid'].' <br /><i class="fa fa-clock-o"></i> ' . date("d.m.Y H:i:s",intval($arr['date'])) . '</td>'.
				'<td >'.$arr['text'].'</td>'.			                        
				'<td >'.$arr['link'].'</td>'.			                        
	                     '</tr>';
	}else{	
		$str1=mb_strtolower($arr['text'],'utf-8');			
		$str2=mb_strtolower($word,'utf-8');
		$mm=explode(",",$str2);
		$flag=0;
		foreach ($mm as $m )
		{
			$str = mb_stristr($str1,$m);
			if($str===false){}else{$flag=1;}
		}		

		if($flag){
	            $html .= '<tr>'.
				'<td >'.$arr['uid'].' <br /><i class="fa fa-clock-o"></i> ' . date("d.m.Y H:i:s",intval($arr['date'])) . '</td>'.
				'<td >'.$arr['text'].'</td>'.			                        
				'<td >'.$arr['link'].'</td>'.			                        
	                     '</tr>';
			}
		
	        }
	}

       $html .= '</table>';

       echo $html;
}

function filter_usr($val,$mas_post,$year){

	$year1=explode(".",$val['bdate']);
	$year2=explode("-",$mas_post['old']);

	if(isset($year1[2]) AND isset($year2[1])){

		$year=$year-$year1[2];
		
		if($year >= $year2[0] AND $year <= $year2[1])
		$is_old=true;
		else 
		$is_old=false;

	}else{$is_old=false;}
	

	if($mas_post['old']!="" AND !$is_old) return false;		
	if($mas_post['sex']!=3 AND $mas_post['sex']!=$val['sex'] ) return false;	
	if($mas_post['country']!="" AND $mas_post['country']!=$val['country']) return false;
	if($mas_post['city']!="" AND $mas_post['city']!=$val['city']) return false;
	if($mas_post['likes']!="" AND $mas_post['likes'] > $val['l']) return false;		
	if($mas_post['reposts']!="" AND $mas_post['reposts'] > $val['r']) return false;		
	if($mas_post['comments']!="" AND $mas_post['comments'] > $val['c']) return false;
	

	return true;
}

function iscross($groups, $ncross)
{
	$usr_mas=array();
  	foreach ($groups as $grp)
  	{
  	  $n_gr=explode( "vk.com/",$grp);
	$url='http://api.vk.com/method/groups.getById?group_id='.$n_gr[1].'&fields=members_count';
	$code = json_decode(request_get($url),true);
	$num_memb = $code['response'][0]['members_count'];
	$idgr = $code['response'][0]['gid'];						
	$n=0;
	
	while($num_memb>$n){
		$url='http://api.vk.com/method/groups.getMembers?group_id='.$idgr.'&count=1000&offset='.$n;
		$code = json_decode(request_get($url),true);
		foreach ($code['response']['users'] as $usrid )
		{
		if(isset($usr_mas[ $usrid ])) $usr_mas[ $usrid ]++;
		else $usr_mas[ $usrid ]=1;
		
		}
	$n+=1000;		
	}
	$usrs=null;
	foreach ($usr_mas as $k=>$v )
	{
	  if($v>=$ncross)$usrs[$k]=$k;
	}
	
  	}
	return $usrs;
}

function getInfo($param,$fb)
{
  try {
  $response = $fb->get($param);
  }
 catch(Facebook\Exceptions\FacebookResponseException $e) { echo 'Graph returned an error: ' . $e->getMessage();  exit;}
 catch(Facebook\Exceptions\FacebookSDKException $e) { echo 'Facebook SDK returned an error: ' . $e->getMessage();  exit;}

return $response;
}

?>