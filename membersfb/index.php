﻿<!DOCTYPE html>
<html>
	<head>


		<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">	
		<script src="../js/jquery-2.1.4.js" type="text/javascript"></script>	
		<title>Members Facebook</title>
	</head>
	<body>
<!-- header -->

<div class="pure-menu pure-menu-horizontal">
    <a href="." class="pure-menu-heading pure-menu-link">vkMon</a>
    <ul class="pure-menu-list">
        <li class="pure-menu-item"><a class="pure-menu-link" href="../settings.php" >Settings</a></li>
        <li class="pure-menu-item"><a class="pure-menu-link" href="/members" >Members</a></li>
	<li class="pure-menu-item"><a class="pure-menu-link" href="/groups" >Groups</a></li>
	<li class="pure-menu-item"><a class="pure-menu-link" href="/membersfb" >Membersfb</a></li>
    </ul>   
      
</div>

<!-- header -->

<?php
//error_reporting(E_ALL);
//ini_set("display_errors", 1);

session_set_cookie_params(0, '/');
session_start();
require_once __DIR__ . '/facebook-php-sdk-v4-5.0.0/src/Facebook/autoload.php';
require_once __DIR__ .'/settings.php';

$fb = new Facebook\Facebook([
  'app_id' => FACEBOOK_APP_ID,
  'app_secret' => FACEBOOK_APP_SECRET,
  'default_graph_version' => 'v2.5',
]);

if(!isset($_SESSION['facebook_access_token']))
{

$helper = $fb->getRedirectLoginHelper();
$permissions = ['email', 'user_likes','user_posts']; // optional
$loginUrl = $helper->getLoginUrl(REDIRECT_URL, $permissions);

header("Location: ".$loginUrl);

}	
	

	if(isset($_POST['url_gr']) OR isset($_POST['url_ev'])){

		if(isset($_POST['url_gr']) AND $_POST['url_gr']!==''){
			include realpath('./active.php');
			include realpath('./view.php');
			}
		if(isset($_POST['url_ev']) AND $_POST['url_ev']!==''){
			include realpath('./event.php');
			include realpath('./view_event.php');
			}
		
	}else{
		
		echo '
	
		<form method="POST" action="" class="pure-form pure-form-aligned">
		    <fieldset>
			<div class="pure-control-group">
				<label>Url групп, страниц:</label>		
		        <input type="text" name = "url_gr" style="width:600px;"><br>		
			</div>
			
			<div hidden  class="pure-control-group">
			<label>Состоит не менее чем ... группах: </label>		
		        <input type="text"  name = "ncross" style="width:100px;"><br>
			</div>

			<div class="pure-control-group">
			<label>Пол: </label>		
		        <select  name = "sex">
			<option value="3">все</option>
			<option value="1">жен</option>
			<option value="2">муж</option>
			</select><br>			
			</div>

			<div hidden  class="pure-control-group">
			<label>Возраст: </label>		
		        <input type="text"  name = "old" placeholder="Например: 18-25"><br>
			</div>
			
			<div  hidden class="pure-control-group">
			<label>Страна: </label>		
		        <input type="text"  name = "country"><br>
			</div>

			<div  hidden class="pure-control-group">
			<label>Город: </label>		
		        <input type="text"  name = "city"><br>
			</div>


			<div class="pure-control-group">
			<label>Лайков: </label>		
		        <input type="text"  name = "likes" style="width:100px;"><br>
			</div>

			<div class="pure-control-group">
			<label>Репостов: </label>		
		        <input type="text"  name = "reposts" style="width:100px;"><br>
			</div>

			<div class="pure-control-group">
			<label>Коментариев: </label>		
		        <input type="text"  name = "comments" style="width:100px;"><br>
			</div>
			
			<div  class="pure-control-group">
			<label>По времени с: </label>		
		        <input type="text"  name = "start_t" placeholder="YYYY-MM-DD h:i:s" style="width:200px;">
			<label>по</label>		
		        <input type="text"  name = "end_t" style="width:200px;"><br>
			</div>	

			<div class="pure-control-group">
			<label>По времени: </label>		
		        <select  name = "date">
			<option value="all">все</option>
			<option value="86400">24 часа</option>
			<option value="604800">1 неделя</option>
			<option value="1209600">2 недели</option>
			<option value="1814400">3 неделя</option>
			<option value="2592000">1 месяц</option>
			<option value="7776000">3 месяца</option>
			<option value="15552000">6 месяцев</option>
			<option value="31536000">1 год</option>
			</select><br>
			</div>				

			<div hidden class="pure-control-group">
			<label>Ищем посты: </label>		
		        <select  name = "who">
			<option value="all">от всех</option>
			<option value="owner">только от имени группы</option>
			<option value="others">не от имени группы</option>
			</select><br>
			</div>

			<div class="pure-control-group">
			<label>Просматривать последних постов: </label>		
		        <input type="text"  name = "num_post" style="width:100px;"><br>
			</div>

			<div class="pure-controls">			
		        <button type="submit" class="pure-button pure-button-primary">Загрузить данные</button>
			</div>
		    </fieldset>
		</form>
		<br><br>

		<form method="POST" action="" class="pure-form pure-form-aligned">
		    <fieldset>
			<div class="pure-control-group">
				<label>Url событий:</label>		
		        <input type="text" name = "url_ev" style="width:600px;"><br>		
			</div>

			<div class="pure-control-group">
			<label>Пол: </label>		
		        <select  name = "sex_ev">
			<option value="3">все</option>
			<option value="1">жен</option>
			<option value="2">муж</option>
			</select><br>			
			</div>
			
			<div  class="pure-control-group">
			<label>Интересуются: </label>		
		        <input type="checkbox"  name = "intr" style="width:100px;"><br>
			</div>
			<div  class="pure-control-group">
			<label>Пойдут: </label>		
		        <input type="checkbox"  name = "sure" style="width:100px;"><br>
			</div>
			<div  class="pure-control-group">
			<label>Приглашенные: </label>		
		        <input type="checkbox"  name = "noreply" style="width:100px;"><br>
			</div>

			<div class="pure-controls">
			 <button type="submit" class="pure-button pure-button-primary">Загрузить данные</button>
			</div>
		    </fieldset>
		</form>

		';
	}
?>



<script>
$('th').click(function(){
    var table = $(this).parents('table').eq(0)
    var rows = table.find('tr:gt(0)').toArray().sort(comparer($(this).index()))
    this.asc = !this.asc
    if (!this.asc){rows = rows.reverse()}
    for (var i = 0; i < rows.length; i++){table.append(rows[i])}
})
function comparer(index) {
    return function(a, b) {
        var valA = getCellValue(a, index), valB = getCellValue(b, index)
        return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.localeCompare(valB)
    }
}
function getCellValue(row, index){ return $(row).children('td').eq(index).html() }
</script>

<style>
th span {text-decoration: none; border-bottom: 1px dotted blue; color: blue !important; cursor: n-resize}
</style>


	</body>
</html>











